<?php


class Server
{
    const PORT = 9501;
    public function port(){
        $shell = "netstat -anp | grep 2>/dev.null ".self::PORT." | grep LISTEN | wc -l";
        //后台运行监控
//        $shell ="nohup /usr/local/php/bin/php /www/wwwroot/live/script/monitor/Server.php > /www/wwwroot/live/script/monitor/a.txt &"
        $result = shell_exec($shell);
        if ($result != 1){
            //发送报警短信 邮件
            echo  date('Ymd H:i:s').'error'.PHP_EOL;
        }else{
            echo  date('Ymd H:i:s').'success'.PHP_EOL;
        }
    }
}

swoole_timer_tick(2000,function($timer_id){
    (new Server())->port();
});
