<?php

namespace app\admin\controller;

use app\common\lib\Util;
use app\common\lib\redis\Predis;


class Live
{
    public function push()
    {
//        if (empty($_GET)) {
//            return Util::show(config('code.error'), 'error');
//        }
//        print_r($_GET);

        //1信息入库 2：推送到直播页面
        //获取redis当前用户
        $teams = [
            1 => [
                'name' => '马刺',
                'logo' => '/live/imgs/team1.png',
            ],
            4 => [
                'name' => '火箭',
                'logo' => '/live/imgs/team2.png',
            ]
        ];
//        print_r($teams[$_GET['team_id']]['logo']);
//        if (!$_GET['team_id'] || $_GET[''])
//        if (!$_GET['team_id']){
//            $title = '直播员';
//            $logo = '';
//        }else{
//            $title = $teams[$_GET['team_id']]['name'];
//            $teams[$_GET['team_id']]['logo'];
//        }
        $data = [
            'type' => intval($_GET['type']),
            'title' => !empty($teams[$_GET['team_id']]['name']) ? $teams[$_GET['team_id']]['name'] : '直播员',
            'logo' => !empty($teams[$_GET['team_id']]['logo']) ? $teams[$_GET['team_id']]['logo'] : '',
            'content' => $_GET['content'],
            'image' => !empty($_GET['image']) ? $_GET['image'] : '',
        ];

        $livedata = [
            'method' => 'pushLive',
            'data'=>$data
        ];
        $_POST['http_server']->task($livedata);
        return Util::show(config('code.success'), 'send ok');

//        $_POST['http_server']->push(4,'date_push_success_mylee');
    }
}