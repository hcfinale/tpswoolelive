<?php


namespace app\common\lib;


class SendSms
{
	/**
	 * 发送验证码
	 * @param $mobile
	 * @param $code
	 * @return bool
	 */
	static function sendCode($mobile,$code){

		//短信内容 $content
        $content = "尊敬的VIP用户您好，验证码为{$code}，请不要泄漏给他人【xxxx】";
        $smsapi = "xxx"; //短信网关
        $user = "xxx"; //短信平台帐号
        $pass = "xxxx"; //短信平台密码
        $sendurl = "http://{$smsapi}/sms.aspx?action=send&account={$user}&password={$pass}&mobile={$mobile}&content=".urlencode($content);
        $result = self::sedverifyCode($sendurl);
        // $result= execResult($result);
        // $xml= "<xml><appid>123456</appid></xml>";//XML文件
        // $objectxml = simplexml_load_string($xml);//将文件转换成 对象
        // $xmljson= json_encode($objectxml );//将对象转换个JSON
        // $xmlarray=json_decode($xmljson,true);//将json转换成数组
        $xml = simplexml_load_string($result);
        $result = json_decode(json_encode($xml),TRUE);
        if ($result['returnstatus'] == 'Success') {
            return true;
        }
        return false;
	}

	/**
	 * 短信处理返回值
	 * @param $result
	 * @return array[]|false|string[]
	 */
	static function execResult($result){
		$result=preg_split("/[,\r\n]/",$result);
		return $result;
	}

	/**
	 * 短信处理返回值
	 * @param $url
	 * @return string
	 */
	static function sedverifyCode($url)
	{
		if(function_exists('file_get_contents'))
		{
			$file_contents = file_get_contents($url);
		}
		else
		{
			$ch = curl_init();
			$timeout = 5;
			curl_setopt ($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, array('Content-Type: application/json; charset=utf-8'));
			$file_contents = curl_exec($ch);
			curl_close($ch);
		}
		return trim($file_contents);
	}
}
