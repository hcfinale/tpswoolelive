<?php


namespace app\common\lib;


class Redis
{
	/**
	 * redis 验证码前缀
	 * @var string
	 */
	public static $pre = 'sms_';
	public static $user = 'user_';

	/**
	 * redis验证码key
	 * @param $phone
	 * @return string
	 */
	public static function smsKey($phone){
		return self::$pre.$phone;
	}

    /**
     * 用户key
     * @param $phone
     * @return string
     */
    public static function userKey($phone){
        return self::$user.$phone;
    }
}