<?php

namespace app\index\controller;

use think\Controller;
use app\common\lib\SendSms;
use app\common\lib\Util;
use app\common\lib\Redis;

class Send extends Controller
{
    public function index()
    {
//        $mobile = request()->get('phone_num', 0, 'intval');
        $mobile = intval($_GET['phone_num']);
        if (empty($mobile)) {
            return Util::show(config('code.error'), '请填写手机号码');
        }
        $code = rand(1000, 9999);
        $taskdata = [
            'method' => 'sendSms',
            'data'=>[
                'phone_number' => $mobile,
                'code' => $code,
            ]
        ];
        $_POST['http_server']->task($taskdata);
        return Util::show(config('code.success'), 'send ok');
//        try {
//            $res = SendSms::sendCode($mobile, $code);
//        } catch (\Exception $exception) {
//            return Util::show(config('code.error'), $exception->getMessage());
//        }
//        if ($res) {
//            $redis = new \Swoole\Coroutine\Redis();
//            $redis->connect(config('redis.host'), config('redis.port'));
//            $redis->set(Redis::smsKey($mobile), $code, config('redis.exp'));
////			return json(['status'=>1,'msg'=>'success']);
//            return Util::show(config('code.success'), '验证码已发送');
//        } else {
//            return Util::show(config('code.error'), '发送验证码失败');
//        }
    }
}