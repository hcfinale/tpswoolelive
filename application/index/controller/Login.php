<?php


namespace app\index\controller;

use app\common\lib\Util;
use app\common\lib\Redis;
use app\common\lib\redis\Predis;

//use think\Controller;

class Login
{
    public function index()
    {
        //需在服务器上安装phpredis拓展
        //获取手机号码
        $phone_num = intval($_GET['phone_num']);
        $code= intval($_GET['code']);
        if (empty($phone_num) || empty($code)){
            return Util::show(config('code.error'),'phone or code is error');
        }
        //获取redis里验证码
        $rcode = Predis::getInstance()->get(Redis::smsKey($phone_num));
        if (!$rcode){
            return Util::show(config('code.error'),'please send code');
        }
        if ($rcode == $code){
            $data = [
                'user'=>$phone_num,
                'srckey' =>md5(Redis::userKey($phone_num)),
                'time'=>time(),
                'islogin'=>true,
            ];
            Predis::getInstance()->set(Redis::userKey($phone_num),$data);
            Predis::getInstance()->delSms(Redis::smsKey($phone_num));
            return Util::show(config('code.success'),'ok');
        }else{

            return Util::show(config('code.error'),'code is error');
        }

    }
}